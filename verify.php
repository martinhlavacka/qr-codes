<?php

	/*

		CREDIT
		---------------------------------------
		Author: 	Martin Hlavacka
		Contact: 	martinhlavacka@outlook.com 
		Date: 		07.04.2018
				
		LICENSE
		-----------------------------------------
		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in all
		copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
		SOFTWARE.

		USAGE
		-----------------------------------------
		01. UPDATE SECRET CODE

	*/


	// IF THIS IS A POST REQUEST
	if(isset($_POST['request'])){

		// STORE PASSED VALUES
		$secret 	= "INSERT-SECRET-CODE";
		$code 		= htmlspecialchars($_POST['request']);

		// CREATE NEW OBJECT
		$verify 	= new Verify($secret, $code);

	}

	// IF THIS IS NOT A POST REQUEST
	else{

		// RETURN ERROR
		http_response_code(500);

	}
		
	// CLASS DEFINITION
	class Verify{

		// PARAMETERS
		private $secret;
		private $code;

		// CONSTRUCTOR
		public function __construct($secret, $code){

			// ASSIGN VALUES
			$this->secret 	= $secret;
			$this->code		= $code;

			// VERIFY CODE
			$this->verifyNow();

		}

		// METHOD FOR CODE VERIFICATION
		public function verifyNow(){
			
			// REQUIRE AUTHENTICATOR
			require_once 'PHPGangsta/GoogleAuthenticator.php';

			// CREATE NEW OBJECT
			$authenticator = new PHPGangsta_GoogleAuthenticator();

			// VERIFY IF THE CODE WORKS
			$result = $authenticator->verifyCode($this->secret, $this->code, 0);

			// GET RESULT
			$this->getResults($result);

		}
		
		// METHOD FOR RESULT
		public function getResults($result){

			// IF RESULT WAS SUCCESFULL
			if($result == 1){
				echo "success";
			}

			// IF AUTHENTICATION FAILED
			else{
				echo "failure";
			}

		}

	}

?>