<!-- jQUERY -->
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

<!-- FIELDS -->
<div class="authentication-fields" id="login-form">
	<input type="number" id="code" name="code" placeholder="Insert code from app" autofocus required>
	<button id="submit" name="authenticator-verify" type="button"><i class="far fa-check-circle"></i>Verify</button>
</div>

<!-- AJAX CALL TO VERIFY CODE -->
<script type="text/javascript">
	$("#submit").on('click', function(){
		var code = $('#code').val();
		if (code.length == 6){
			$.post(
				'verify.php', 
				{request: code},
				function(result){
					if(result == "success"){
						$("#code").val("");
						alert("Code worked")
					}
					else{
						$("#code").val("");
						alert("Code didn´t work");
					}
				}
			);
		}
	});
</script>

